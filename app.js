const express = require("express");
const db = require("./config");
const {
  collection,
  addDoc,
  setDoc,
  doc,
  query,
  where,
  getDocs,
  getDoc,
} = require("firebase/firestore/lite");
const { onSnapshot } = require("firebase/firestore");
var nodemailer = require("nodemailer");
const stripe = require('stripe')('sk_test_51Jyiv3A35MqmL7JolflhCLbd9nDh4kA4Updr7XLEjohZNu6qmM1rvJmXQiABFIvKDFvYe8ZLuzU05bif1C4QDu6w00P6U9fsDT');

var cors = require("cors");
const app = express();

app.use(express.json());
app.use(cors());
const port = process.env.PORT || 5000;
//const port = 5000;
app.listen(port);

// Create New User with linked account
app.post("/createLinkedUser", async (req, res) => {
  try {
    const data = req.body;
    await setDoc(doc(db, "users", data.uid), data.data);
    res.send({ msg: "user added", code: 200 });
    res.end();
  } catch (e) {
    res.send({ msg: e, code: 500 });
    res.end();
  }
});

//Get user's info by ID
app.post("/user", async (req, res) => {
  try {
    const data = req.body;
    const docRef = doc(db, "users", data.uid);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      res.send({ data: docSnap.data(), msg: "success", code: 200 });
      res.end();
    } else {
      res.send({ data: {}, msg: "No such document!", code: 404 });
      res.end();
    }
  } catch (e) {
    res.send({ msg: e, code: 500 });
    res.end();
  }
});

//Get user's info by email
app.post("/userByEmail", async (req, res) => {
  try {
    const data = req.body;
    const q = query(collection(db, "users"), where("email", "==", data.email));
    const querySnapshot = await getDocs(q);
    if (querySnapshot.size > 0) {
      res.send({ data: {}, msg: "User Exists", code: 200 });
      res.end();
    } else {
      res.send({ data: {}, msg: "No such user!", code: 404 });
      res.end();
    }
  } catch (e) {
    res.send({ msg: e, code: 500 });
    res.end();
  }
});

//Create New User with Email
app.post("/createUserWithEmail", async (req, res) => {
  try {
    const data = req.body;
    await setDoc(doc(db, "users", data.id), data);
    res.send({ msg: "user added", code: 200 });
    res.end();
  } catch (e) {
    res.send({ msg: e, code: 500 });
    res.end();
  }
});

//Send Verification email
app.post("/verifyEmail", async (req, res) => {
  try {
    const data = req.body;
    const q = query(collection(db, "users"), where("email", "==", data.email));
    const querySnapshot = await getDocs(q);
    if (querySnapshot.size < 1) {
      console.log("code sending");
      var transport = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
          user: "2b0635ad0a1ea0",
          pass: "10cb38a21ec08a",
        },
      });
      var code = Math.floor(100000 + Math.random() * 600000);
      var mailOptions = {
        from: "support@rentacar.com",
        to: data.email,
        subject: "Verification Code",
        html: "<h1>Rental Car </h1>Your Verification code is " + code,
      };

      transport.sendMail(mailOptions, function (error, info) {
        if (error) {
          res.send({ msg: error.message, code: 500, verification_code: null });
          res.end();
        } else {
          res.send({ msg: "Email sent", verification_code: code, status: 200 });
          res.end();
        }
      });
    } else {
      res.send({
        msg: "Email_already_verified",
        verification_code: null,
        status: 200,
      });
      res.end();
    }
  } catch (error) {
    console.log("user already exits");
    res.send({ msg: error, code: 500, verification_code: null });
    res.end();
  }
});

//Post a car
app.post("/postACar", async (req, res) => {
  try {
    const data = req.body;
    await addDoc(collection(db, "cars"), data);
    res.send({ msg: "car added", code: 200 });
    res.end();
  } catch (e) {
    console.log(e.message)
    res.send({ msg: e, code: 500 });
    res.end();
  }
});

//Get Host Cars
app.get("/getHostCars", async (req, res) => {
  try {
    const email = req.query.email;
    const q = query(collection(db, "cars"), where("host", "==", email));
    const querySnapshot = await getDocs(q);
    if (querySnapshot.size > 0) {
      let cars = [];
      querySnapshot.forEach((doc) => {
        cars.push(doc.data());
      });
      res.send({ msg: "Cars Found", code: 200, data: cars });
      res.end();
    } else {
      res.send({ msg: "No Cars Found", code: 201, data: [] });
      res.end();
    }
  } catch (e) {
    res.send({ msg: e, code: 500, data: null });
    res.end();
    console.log(e);
  }
});

//Get All Cars
app.post("/getCars", async (req, res) => {
  try {
    const querySnapshot = await getDocs(collection(db, "cars"));
    if (querySnapshot.size > 0) {
      let cars = [];
      querySnapshot.forEach((doc) => {
        cars.push(doc.data());
      });
      res.send({ msg: "Cars Found", code: 200, data: cars });
      res.end();
    } else {
      res.send({ msg: "No Cars Found", code: 404, data: [] });
      res.end();
    }
  } catch (e) {
    res.send({ msg: e, code: 500, data: null });
    res.end();
    console.log(e);
  }
});

//Get Messages
app.post("/getMessages", async (req, res) => {
  try {
    const querySnapshot = await getDocs(collection(db, "messages"));
    if (querySnapshot.size > 0) {
      let convo = [];
      querySnapshot.forEach((doc) => {
        convo.push(doc.data());
      });
      res.send({ msg: "msgs Found", code: 200, data: convo });
      res.end();
    } else {
      res.send({ msg: "No msgs Found", code: 404, data: [] });
      res.end();
    }
  } catch (e) {
    res.send({ msg: e, code: 500, data: null });
    res.end();
    console.log(e);
  }
});

//Send a message
app.post("/sendMessage", async (req, res) => {
  try {
    const data = req.body;
    await addDoc(collection(db, "messages"), data);
    res.send({ msg: "message added", code: 200 });
    res.end();
  } catch (e) {
    console.log(e.message)
    res.send({ msg: e, code: 500 });
    res.end();
  }
});

//Delete a message
app.post("/deleteAMessage", async (req, res) => {
  try {
    const data = req.body;
    await addDoc(collection(db, "messages"), data);
    res.send({ msg: "message deleted", code: 200 });
    res.end();
  } catch (e) {
    console.log(e.message)
    res.send({ msg: e, code: 500 });
    res.end();
  }
});


app.use('/create-payment-intent', async (req, res) => {
  // Set your secret key. Remember to switch to your live secret key in production.
  // See your keys here: https://dashboard.stripe.com/apikeys
  try {
    const data = req.body;
    console.log(data)
    const paymentIntent = await stripe.paymentIntents.create({
      amount: data.amount,
      currency: 'usd',
    });
    const clientSecret = paymentIntent.client_secret
    res.send({ clientSecret: clientSecret, code: 200 });
    res.end();
  } catch (e) {
    console.log(e.message)
    res.send({ msg: e.message, code: e.code });
    res.end();
  }
  // Pass the client secret to the client
})



// Put this code at the end of code always
app.use("/", async (req, res) => {
  res.send("<h1>Error 404 Not Found !</h1>");
  res.end();
});
// Put this code at the end of code always
