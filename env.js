const firebase = require("firebase/app");
const { getFirestore, collection } = require("firebase/firestore/lite");
const firebaseConfig = {
  apiKey: "AIzaSyCB4nfGXThRDBed7lc_hLx7TwgEJ7STIp8",
  authDomain: "rental-app-898c6.firebaseapp.com",
  projectId: "rental-app-898c6",
  storageBucket: "rental-app-898c6.appspot.com",
  messagingSenderId: "397031010102",
  appId: "1:397031010102:web:fffddaa3f6fddede2f4d97",
  measurementId: "G-Y59BND4SZR",
};

// Initialize Firebase
var database = firebase.initializeApp(firebaseConfig);

//Database Firestore
const db = getFirestore(database);

//Authenticate users Login With Email and password

module.exports = db;
